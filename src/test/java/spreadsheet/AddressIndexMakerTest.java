package spreadsheet;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class AddressIndexMakerTest {
    @Test
    public void indexFromRowColumn() {
        assertEquals(0, new AddressIndexMaker(10).indexFromRowColumn((byte) 0, 0));
        assertEquals(9, new AddressIndexMaker(10).indexFromRowColumn((byte) 0, 9));
        assertEquals(16, new AddressIndexMaker(11).indexFromRowColumn((byte) 1, 5));
    }

    @Test
    public void indexFromCellName() {
        assertEquals(0, new AddressIndexMaker(10).indexFromCellName('A', 1));
        assertEquals(260 - 1, new AddressIndexMaker(10).indexFromCellName('Z', 10));
    }

    @Test
    public void decode() {
        assertArrayEquals(new int[]{25, 9}, new AddressIndexMaker(10).decode(259));
        assertArrayEquals(new int[]{0, 0}, new AddressIndexMaker(10).decode(0));
        assertArrayEquals(new int[]{0, 9}, new AddressIndexMaker(10).decode(9));
        assertArrayEquals(new int[]{1, 5}, new AddressIndexMaker(11).decode(16));
    }

    @Test
    public void decodeToString() {
        assertEquals("A1", new AddressIndexMaker(10).decodeToString(0));
        assertEquals("Z1234567", new AddressIndexMaker(9999999).decodeToString(251234541));
    }
}