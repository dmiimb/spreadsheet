package spreadsheet;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import spreadsheet.cell.ExpressionCellFactory;
import spreadsheet.reader.Reader;
import spreadsheet.reader.SpreadsheetReaderException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SpreadsheetTest {
    private Spreadsheet sp;
    private AddressIndexMaker addressIndexMaker;

    @Before
    public void setUp() {
        AddressIndexMakerFactory addressIndexMakerFactory = mock(AddressIndexMakerFactory.class);
        addressIndexMaker = mock(AddressIndexMaker.class);
        when(addressIndexMakerFactory.create(anyInt())).thenReturn(addressIndexMaker);

        ExpressionCellFactory expressionCellFactory = mock(ExpressionCellFactory.class);
        sp = new Spreadsheet(addressIndexMakerFactory, expressionCellFactory);
    }

    @Test
    public void getValueByAddress() throws SpreadsheetReaderException, SpreadsheetException {

        when(addressIndexMaker.indexFromRowColumn((byte) 0, 0)).thenReturn(1);
        when(addressIndexMaker.indexFromRowColumn((byte) 0, 1)).thenReturn(2);
        when(addressIndexMaker.indexFromRowColumn((byte) 0, 2)).thenReturn(3);
        when(addressIndexMaker.indexFromRowColumn((byte) 1, 0)).thenReturn(4);
        when(addressIndexMaker.indexFromRowColumn((byte) 1, 1)).thenReturn(5);
        when(addressIndexMaker.indexFromRowColumn((byte) 1, 2)).thenReturn(6);

        when(addressIndexMaker.decode(1)).thenReturn(new int[]{0, 0});
        when(addressIndexMaker.decode(2)).thenReturn(new int[]{0, 1});
        when(addressIndexMaker.decode(3)).thenReturn(new int[]{0, 2});
        when(addressIndexMaker.decode(4)).thenReturn(new int[]{1, 0});
        when(addressIndexMaker.decode(5)).thenReturn(new int[]{1, 1});
        when(addressIndexMaker.decode(6)).thenReturn(new int[]{1, 2});

        Reader r = mock(Reader.class);
        when(r.readLine()).thenAnswer(new Answer() {
            private int c = 0;

            @Override
            public Object answer(InvocationOnMock invocationOnMock) {
                c++;
                if (1 == c) {
                    return new String[]{"1", "2", "3"};
                } else if (2 == c) {
                    return new String[]{"4", "5", "6"};
                } else {
                    return null;
                }
            }
        });
        sp.importData(r);

        assertEquals(2.0, sp.getValueByAddress(2), 0.00000001);
        assertEquals(4.0, sp.getValueByAddress(4), 0.00000001);
        assertEquals(6.0, sp.getValueByAddress(6), 0.00000001);
    }
}