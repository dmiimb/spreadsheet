package spreadsheet.cell;

import org.junit.Ignore;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ExpressionTest {
    private final static Map<String, Double> NOREFS = new HashMap<>(0);
    private static final double DELTA = 0.0000000001;

    @Test
    public void oneNumber() throws InvalidExpressionException {
        assertEquals(123, new Expression("123").calculate(NOREFS), DELTA);
        assertEquals(-123, new Expression("-123").calculate(NOREFS), DELTA);
        assertEquals(123, new Expression("+123").calculate(NOREFS), DELTA);
    }

    @Test
    public void oneOperation() throws InvalidExpressionException {
        assertEquals(123 * 321, new Expression("123*321").calculate(NOREFS), DELTA);
        assertEquals(123 + 321, new Expression("123+321").calculate(NOREFS), DELTA);
        assertEquals(123 - 321, new Expression("123-321").calculate(NOREFS), DELTA);
        assertEquals(123.0 / -321, new Expression("123/-321").calculate(NOREFS), DELTA);
    }

    @Test
    public void multiOperations() throws InvalidExpressionException {
        assertEquals(2 + 2 * 2, new Expression("2+2*2").calculate(NOREFS), DELTA);
        assertEquals(2 - 2 * 2, new Expression("2-2*2").calculate(NOREFS), DELTA);
        assertEquals(3 * 2 + 4 * 5, new Expression("3*2+4*5").calculate(NOREFS), DELTA);
        assertEquals(1 - 2.0 / 2, new Expression("1-2/2").calculate(NOREFS), DELTA);
        assertEquals(2 * 2.0 / 2 - 3 * 3.0 / 3, new Expression("2*2/2-3*3/3").calculate(NOREFS), DELTA);
    }

    @Test
    public void oneOperationNegativeValue() throws InvalidExpressionException {
        assertEquals(2 * -2, new Expression("2*-2").calculate(NOREFS), DELTA);
        assertEquals(-3.0 / -3, new Expression("-3/-3").calculate(NOREFS), DELTA);
        assertEquals(2 + -5, new Expression("2+-5").calculate(NOREFS), DELTA);
        assertEquals(0, new Expression("-1--1").calculate(NOREFS), DELTA);
    }

    @Test
    public void soloParentheses() throws InvalidExpressionException {
        assertEquals(2, new Expression("(2)").calculate(NOREFS), DELTA);
        assertEquals(-5, new Expression("((-5))").calculate(NOREFS), DELTA);
    }

    @Test
    public void singleParentheses() throws InvalidExpressionException {
        assertEquals((2 + 2) * 2, new Expression("(2+2)*2").calculate(NOREFS), DELTA);
        assertEquals(3 * (2 + 2), new Expression("3*(2+2)").calculate(NOREFS), DELTA);
        assertEquals(2 - 3 * (2.0 / 2), new Expression("2-3*(2/2)").calculate(NOREFS), DELTA);
    }

    @Test
    public void serialParentheses() throws InvalidExpressionException {
        assertEquals((2 + 2) + (3 * 3), new Expression("(2+2)+(3*3)").calculate(NOREFS), DELTA);
        assertEquals((2.0 + 2) / (3.0 + 3), new Expression("(2+2)/(3+3)").calculate(NOREFS), DELTA);
    }

    @Test
    @Ignore
    public void unaryMinus() throws InvalidExpressionException {
        // removed for performance
        assertEquals(-(2.0 + 2), new Expression("-(2+2)").calculate(NOREFS), DELTA);
        assertEquals(1 - (2.0 + 2), new Expression("1-(2+2)").calculate(NOREFS), DELTA);
        assertEquals((2.0 + 2) / -(3 + 4), new Expression("(2+2)/-(3+4)").calculate(NOREFS), DELTA);
        assertEquals((2.0 + 2) + -(3 + 4), new Expression("(2+2)+-(3+4)").calculate(NOREFS), DELTA);
        assertEquals((2.0 + 2) - -(3 + 4), new Expression("(2+2)--(3+4)").calculate(NOREFS), DELTA);
    }

    @Test
    @Ignore
    public void multiUnaryOperators() throws InvalidExpressionException {
        // removed for performance
        assertEquals(+-+-+ + +- - -8, new Expression("+-+-+++---8").calculate(NOREFS), DELTA);
        assertEquals((1 + -+2) + - -+(7 - - - -8), new Expression("(1+-+2)+--+(7----8)").calculate(NOREFS), DELTA);
        assertEquals(+-+-+ + +- - -(- -+ +-8), new Expression("+-+-+++---(--++-8)").calculate(NOREFS), DELTA);
    }

    @Test
    @Ignore
    public void unaryPlus() throws InvalidExpressionException {
        // removed for performance
        assertEquals(+(2.0 + 2 - 8), new Expression("+(2+2-8)").calculate(NOREFS), DELTA);
        assertEquals((2.0 + 2) / +(3 + 4), new Expression("(2+2)/+(3+4)").calculate(NOREFS), DELTA);
        assertEquals((2.0 + 2) + +(3 + 4), new Expression("(2+2)++(3+4)").calculate(NOREFS), DELTA);
        assertEquals((2.0 + 2) - +(3 + 4), new Expression("(2+2)-+(3+4)").calculate(NOREFS), DELTA);
    }

    @Test
    public void nestedParentheses() throws InvalidExpressionException {
        assertEquals(
                (2.0 / -(2 + 3) - 77) * (23 + 77) - (99.0 / (56 - 45 * (2 + 3.0 / 4)) - 49),
                new Expression("(2/-(2+3)-77)*(23+77)-(99/(56-45*(2+3/4))-49)").calculate(NOREFS),
                DELTA
        );
    }

    @Test
    public void usingReferences() throws InvalidExpressionException {
        Map<String, Double> refs = new HashMap<>(3);
        refs.put("R1", 2.0);
        refs.put("T99", 1.0);
        refs.put("Z2", 3.0);
        assertEquals(
                2.0 + 1.0 / 3.0,
                new Expression("R1+T99/Z2").calculate(refs),
                DELTA
        );
    }

    @Test
    public void divByZero() throws InvalidExpressionException {
        assertEquals(
                Double.POSITIVE_INFINITY,
                new Expression("10/0").calculate(NOREFS),
                DELTA
        );
    }

    @Test(expected = InvalidExpressionException.class)
    public void emptyExpression() throws InvalidExpressionException {
        new Expression("").calculate(NOREFS);
    }

    @Test(expected = InvalidExpressionException.class)
    public void invalidNumber() throws InvalidExpressionException {
        new Expression("123*1z3").calculate(NOREFS);
    }

    @Test(expected = InvalidExpressionException.class)
    public void noOperandMinus() throws InvalidExpressionException {
        new Expression("2112-").calculate(NOREFS);
    }

    @Test(expected = InvalidExpressionException.class)
    public void noOperandAny() throws InvalidExpressionException {
        new Expression("2112*").calculate(NOREFS);
    }

    @Test(expected = InvalidExpressionException.class)
    public void openParentheses1() throws InvalidExpressionException {
        new Expression("2112+(").calculate(NOREFS);
    }

    @Test(expected = InvalidExpressionException.class)
    public void openParentheses2() throws InvalidExpressionException {
        new Expression("2112+)").calculate(NOREFS);
    }

    @Test(expected = InvalidExpressionException.class)
    public void openParentheses3() throws InvalidExpressionException {
        new Expression("(2112").calculate(NOREFS);
    }

    @Test(expected = InvalidExpressionException.class)
    public void openParentheses4() throws InvalidExpressionException {
        new Expression("123+2112)").calculate(NOREFS);
    }

    @Test(expected = InvalidExpressionException.class)
    public void noOpParentheses() throws InvalidExpressionException {
        new Expression("233(555").calculate(NOREFS);
    }
}
