package spreadsheet.cell;

import org.junit.Before;
import org.junit.Test;
import spreadsheet.AddressIndexMaker;
import spreadsheet.Spreadsheet;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ExpressionCellTest {
    private AddressIndexMaker addressIndexMaker;
    private Expression ex;

    @Before
    public void setUp() {
        addressIndexMaker = mock(AddressIndexMaker.class);
        ex = mock(Expression.class);
    }

    @Test
    public void oneRef() throws InvalidExpressionException {
        when(ex.getExpression()).thenReturn("123+R789".toCharArray());
        when(addressIndexMaker.indexFromCellName('R', 789)).thenReturn(100);

        ExpressionCell ec = new ExpressionCell(addressIndexMaker, 123, ex);
        assertEquals(1, ec.getMissingReferencesCnt());
    }

    @Test
    public void threeRefs() throws InvalidExpressionException {
        when(ex.getExpression()).thenReturn("U2/Y1+123+R789/Y1*(126)-R789*U2".toCharArray());
        when(addressIndexMaker.indexFromCellName('U', 2)).thenReturn(100);
        when(addressIndexMaker.indexFromCellName('Y', 1)).thenReturn(200);
        when(addressIndexMaker.indexFromCellName('R', 789)).thenReturn(300);

        ExpressionCell ec = new ExpressionCell(addressIndexMaker, 123, ex);
        assertEquals(3, ec.getMissingReferencesCnt());
    }

    @Test
    public void reCheckReferences() throws InvalidExpressionException {
        when(ex.getExpression()).thenReturn("U2/Y1-T99+123+R789/Y1*(126)-R789*U2".toCharArray());
        when(addressIndexMaker.indexFromCellName('U', 2)).thenReturn(100);
        when(addressIndexMaker.indexFromCellName('Y', 1)).thenReturn(200);
        when(addressIndexMaker.indexFromCellName('R', 789)).thenReturn(300);
        when(addressIndexMaker.indexFromCellName('T', 99)).thenReturn(400);

        ExpressionCell ec = new ExpressionCell(addressIndexMaker, 123, ex);

        Spreadsheet sp = mock(Spreadsheet.class);
        when(sp.getValueByAddress(100)).thenReturn(1.0);
        when(sp.getValueByAddress(200)).thenReturn(null);
        when(sp.getValueByAddress(300)).thenReturn(2.0);
        when(sp.getValueByAddress(400)).thenReturn(3.0);

        ec.reCheckReferences(sp);

        assertEquals(1, ec.getMissingReferencesCnt());
    }
}