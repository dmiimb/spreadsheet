package spreadsheet.cell;


import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.Stack;

class Expression {
    @Getter
    private final char[] expression;
    private int pos = 0;

    Expression(String expression) {
        this.expression = expression
                .replace("/-(", "*-1/(")
                .replace("(-(", "(-1*(")
                .replace("/+(", "/(")
                .toCharArray();
    }

    @Override
    public String toString() {
        return String.valueOf(expression);
    }

    double calculate(Map<String, Double> refs) throws InvalidExpressionException {
        pos = 0;
        Stack<Double> operands = new Stack<>();
        Stack<Character> operators = new Stack<>();

        // skip first unary plus
        if (pos < expression.length && '+' == expression[pos]) {
            pos++;
        }

        while (pos < expression.length && '(' == expression[pos]) {
            pos++;
            operators.push('(');
        }
        double number = nextNumber(refs);
        operands.push(number);

        while (pos < expression.length) {
            while (pos < expression.length && ')' == expression[pos]) {
                Double result = sum(operands, operators, '(');
                addOperand(operands, operators, result);
                pos++;
            }
            if (pos >= expression.length) {
                break;
            }
            if (isNotOperator(expression[pos])) {
                throw new InvalidExpressionException("Wrong expression: " + Arrays.toString(expression));
            }
            operators.push(expression[pos++]);
            while (pos < expression.length && '(' == expression[pos]) {
                pos++;
                operators.push('(');
            }
            number = nextNumber(refs);
            addOperand(operands, operators, number);
        }

        return sum(operands, operators, null);
    }

    private void addOperand(Stack<Double> operands, Stack<Character> operators, Double val) {
        if (operators.empty()) {
            operands.push(val);
            return;
        }
        char lastOperator = operators.peek();
        if ('*' == lastOperator || '/' == lastOperator) {
            operators.pop();
            Double lastOperand = operands.pop();
            val = ('*' == lastOperator)
                    ? lastOperand * val
                    : lastOperand / val;
            lastOperator = operators.empty() ? '+' : operators.peek();
        }
        if ('-' == lastOperator) {
            val = -val;
            operators.pop();
            operators.push('+');
        }
        operands.push(val);
    }

    private Double sum(Stack<Double> operands, Stack<Character> operators, Character begin)
            throws InvalidExpressionException {
        Double result = operands.pop();
        Character operator = operators.empty() ? null : operators.pop();
        while (operator != begin) {
            if (null == operator || operator != '+') {
                throw new InvalidExpressionException("Wrong expression: " + Arrays.toString(expression));
            }
            Double nextOperand = operands.pop();
            result += nextOperand;
            operator = operators.empty() ? null : operators.pop();
        }
        return result;
    }

    private double nextNumber(Map<String, Double> refs) throws InvalidExpressionException {
        int i = pos;
        if (i >= expression.length) {
            throw new InvalidExpressionException("Wrong expression: " + Arrays.toString(expression));
        }
        if ('-' == expression[i] || '+' == expression[i]) {
            i++;
        }
        while (i < expression.length && isNotOperator(expression[i]) && expression[i] != ')') {
            i++;
        }
        try {
            double result;
            if (expression[pos] > '9') {
                result = refs.get(
                        String.valueOf(Arrays.copyOfRange(expression, pos, i))
                );
            } else {
                result = Integer.parseInt(
                        String.valueOf(Arrays.copyOfRange(expression, pos, i))
                );
            }
            pos = i;
            return result;
        } catch (NumberFormatException e) {
            throw new InvalidExpressionException(
                    "Invalid numeric: " + String.valueOf(Arrays.copyOfRange(expression, pos, i))
            );
        }
    }

    private boolean isNotOperator(char c) {
        return c < '*' || c > '/';
    }
}
