package spreadsheet.cell;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import spreadsheet.AddressIndexMaker;
import spreadsheet.Spreadsheet;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class ExpressionCell {
    private final AddressIndexMaker addressIndexMaker;
    @Getter
    private final int address;
    private final Expression expression;
    private Double value;
    private Map<Integer, Boolean> missingReferences;
    @Getter
    private short missingReferencesCnt;

    ExpressionCell(AddressIndexMaker addressIndexMaker, int address, Expression expression)
            throws InvalidExpressionException {
        this.addressIndexMaker = addressIndexMaker;
        this.address = address;
        this.expression = expression;

        ReferencesBuilder builder = new ReferencesBuilder(expression.getExpression());
        missingReferences = builder.buildRefs();
        missingReferencesCnt = (short) missingReferences.size();
    }

    public boolean isNotCalculated() {
        return null == value;
    }

    public Double getValue(Spreadsheet target) throws InvalidExpressionException {
        if (null == value) {
            Map<String, Double> refs = missingReferences.entrySet().stream()
                    .collect(Collectors.toMap(
                            e -> addressIndexMaker.decodeToString(e.getKey()),
                            e -> target.getValueByAddress(e.getKey())
                    ));
            value = expression.calculate(refs);
        }
        return value;
    }

    public void reCheckReferences(Spreadsheet target) {
        missingReferencesCnt = 0;
        for (Map.Entry<Integer, Boolean> entry : missingReferences.entrySet()) {
            if (!entry.getValue()) {
                if (target.getValueByAddress(entry.getKey()) == null) {
                    missingReferencesCnt++;
                } else {
                    entry.setValue(false);
                }
            }
        }
    }

    @RequiredArgsConstructor
    private class ReferencesBuilder {
        private final char[] expression;
        private int pos;

        Map<Integer, Boolean> buildRefs() throws InvalidExpressionException {
            Map<Integer, Boolean> missingReferences = new HashMap<>();
            pos = 0;
            while (pos < expression.length) {
                char c = expression[pos];
                if ('A' <= c && c <= 'Z') {
                    int key = addressIndexMaker.indexFromCellName(c, getNumber());
                    missingReferences.put(key, false);
                } else {
                    pos++;
                }
            }
            return missingReferences;
        }

        private int getNumber() throws InvalidExpressionException {
            int i = pos + 1;
            while (i < expression.length && '0' <= expression[i] && expression[i] <= '9') {
                i++;
            }
            if (i == pos + 1) {
                throw new InvalidExpressionException("Wrong expression: " + Arrays.toString(expression));
            }
            int result = Integer.parseInt(
                    String.valueOf(Arrays.copyOfRange(expression, pos + 1, i))
            );
            pos = i;
            return result;
        }
    }
}
