package spreadsheet.cell;

public class InvalidExpressionException extends Exception {
    InvalidExpressionException(String s) {
        super(s);
    }
}
