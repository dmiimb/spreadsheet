package spreadsheet.cell;

import org.springframework.stereotype.Component;
import spreadsheet.AddressIndexMaker;

@Component
public class ExpressionCellFactory {
    public ExpressionCell create(AddressIndexMaker addressIndexMaker, int index, String expression)
            throws InvalidExpressionException {
        return new ExpressionCell(addressIndexMaker, index, new Expression(expression));
    }
}
