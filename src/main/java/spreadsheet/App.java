package spreadsheet;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import spreadsheet.generator.Generator;
import spreadsheet.reader.FileReader;
import spreadsheet.writer.FileWriter;

@Log4j2
@RequiredArgsConstructor
@SpringBootApplication
public class App implements CommandLineRunner {
    private final Generator generator;
    private final SpreadsheetEvaluator spreadsheetEvaluator;

    public static void main(String[] args) throws IllegalArgumentException {
        SpringApplication.run(App.class, args);

    }

    private String getValue(String[] args, int i) throws IllegalArgumentException {
        if (i < args.length) {
            if (args[i].charAt(0) != '-') {
                return args[i];
            }
        }
        throw new IllegalArgumentException("Expected argument value after: " + args[i - 1]);
    }

    @Override
    public void run(String... args) {
        String inputFile = null;
        String outputFile = null;
        Integer generateSize = null;
        byte expPercentage = 10;
        boolean toExcel = false;
        try {
            for (int i = 0; i < args.length; i++) {
                switch (args[i]) {
                    case "-i":
                        inputFile = getValue(args, i + 1);
                        i++;
                        break;
                    case "-o":
                        outputFile = getValue(args, i + 1);
                        i++;
                        break;
                    case "-g":
                        generateSize = Integer.parseInt(getValue(args, i + 1));
                        i++;
                        break;
                    case "-er":
                        expPercentage = Byte.parseByte(getValue(args, i + 1));
                        i++;
                        break;
                    case "--excel":
                        toExcel = true;
                        i++;
                        break;
                }
            }
        } catch (IllegalArgumentException e) {
            inputFile = null;
            outputFile = null;
            System.out.println(e.getMessage());
        }
        if (generateSize != null && outputFile != null) {
            if (toExcel) {
                generator.generateTwo(generateSize, expPercentage, outputFile);
            } else {
                generator.generateSingle(generateSize, expPercentage, outputFile);
            }
        } else {
            if (null == inputFile || null == outputFile) {
                System.out.println("Usage: java -jar target/spreadsheet.jar -i inputfile.csv -o outputfile.csv");
                System.exit(1);
            }

            long startTime = System.nanoTime();
            try {
                spreadsheetEvaluator.evaluate(new FileReader(inputFile), new FileWriter(outputFile));
            } catch (SpreadsheetException e) {
                log.error("Failed to evaluate: ", e);
            } catch (SpreadsheetLoopException e) {
                log.error(e.getMessage());
            }
            float time = (float) (System.nanoTime() - startTime) / 1000000;
            log.info("Taken: " + time);
            System.out.println("Done. Total time: " + time + " ms");
        }
    }
}
