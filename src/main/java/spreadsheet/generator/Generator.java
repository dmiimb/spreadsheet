package spreadsheet.generator;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import spreadsheet.AddressIndexMaker;
import spreadsheet.AddressIndexMakerFactory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static spreadsheet.Spreadsheet.MAX_ROWS;

@Log4j2
@RequiredArgsConstructor
@Component
public class Generator {
    final private static byte ALPHABET_SIZE = 26;
    final private static byte MAX_PERCENTAGE = 100;
    final private static String[] OPERATORS = {"-", "+", "*", "/"};
    private final AddressIndexMakerFactory addressIndexMakerFactory;
    private final Pattern p = Pattern.compile("[A-Z][0-9]+");
    private final Random random = new Random();
    private AddressIndexMaker addressIndexMaker;
    private int[] ready;
    private int r;

    private StringBuffer genExpression() {
        int operands = random.nextInt(3) + 1;
        StringBuffer result = new StringBuffer("=");
        result.append(genSubExpression(random.nextInt(2)));
        for (int t = 1; t < operands; t++) {
            result.append(genOperator());
            result.append(genSubExpression(random.nextInt(2)));
        }
        return result;
    }

    private StringBuffer genSubExpression(int level) {
        if (0 == level) {
            return genOperand();
        }
        StringBuffer result = new StringBuffer("(");
        result.append(genSubExpression(level - 1));
        result.append(genOperator());
        result.append(genSubExpression(level - 1));
        result.append(")");
        return result;
    }

    private StringBuffer genOperand() {
        if (random.nextInt(10) < 8) {
            return new StringBuffer(genNumOperand());
        } else {
            int p = random.nextInt(r);
            int[] position = addressIndexMaker.decode(ready[p]);
            StringBuffer result = new StringBuffer(
                    String.valueOf((char) (position[0] + 'A'))
            );
            result.append((position[1] + 1));
            return result;
        }
    }

    private String genNumOperand() {
        return Integer.valueOf(random.nextInt(1000) - 500).toString();
    }

    private StringBuffer genOperator() {
        return new StringBuffer(OPERATORS[random.nextInt(OPERATORS.length)]);
    }

    private String toExcel(String ins) {
        StringBuffer result = new StringBuffer();
        Matcher matcher = p.matcher(ins);
        while (matcher.find()) {
            String g = matcher.group();
            int col = Integer.parseInt(g.substring(1)) - 1;
            StringBuilder s = new StringBuilder();
            s.append(g.charAt(0) - ALPHABET_SIZE);
            do {
                int next = col / ALPHABET_SIZE;
                char c = (char) ((col - next * ALPHABET_SIZE) + 'A');
                s.insert(0, c);
                col = next;
            } while (col >= ALPHABET_SIZE);
            if (col - 1 >= 0) {
                s.insert(0, (char) (col - 1 + 'A'));
            }
            matcher.appendReplacement(result, s.toString());
        }
        matcher.appendTail(result);
        return result.toString();
    }

    public void generateSingle(int size, byte expPercentage, String filename) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
            generate(size, expPercentage, writer, null);
        } catch (IOException e) {
            log.error("Write file failed", e);
        }
    }

    public void generateTwo(int size, byte expPercentage, String filename) {
        String excelFilename = filename.contains(".csv")
                ? filename.replace(".csv", "_excel.csv")
                : filename + "_excel";
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
             BufferedWriter exWriter = new BufferedWriter(new FileWriter(excelFilename))) {
            generate(size, expPercentage, writer, exWriter);
        } catch (IOException e) {
            log.error("Write file failed", e);
        }
    }

    private void generate(int size, byte expPercentage, BufferedWriter writer, BufferedWriter exWriter) throws IOException {
        String[][] sps = new String[MAX_ROWS][size];
        addressIndexMaker = addressIndexMakerFactory.create(size);
        ready = new int[MAX_ROWS * size];
        r = 0;

        // numbers
        for (byte i = 0; i < MAX_ROWS; i++) {
            for (int j = 0; j < size; j++) {
                if (random.nextInt(MAX_PERCENTAGE / 10) == 0) {
                    sps[i][j] = genNumOperand();
                    ready[r++] = addressIndexMaker.indexFromRowColumn(i, j);
                }
            }
        }
        log.info("Initial numbers done " + r);

        // references
        for (byte i = 0; i < MAX_ROWS; i++) {
            for (int j = 0; j < size; j++) {
                if (null == sps[i][j] && random.nextInt(MAX_PERCENTAGE) == 0) {
                    sps[i][j] = "=" + genOperand();
                    ready[r++] = addressIndexMaker.indexFromRowColumn(i, j);
                }
            }
        }
        log.info("References done " + r);

        for (int k = 0; k < 5; k++) {
            for (byte i = 0; i < MAX_ROWS; i++) {
                for (int j = 0; j < size; j++) {
                    if (null == sps[i][j] && random.nextInt(MAX_PERCENTAGE / expPercentage) == 0) {
                        sps[i][j] = genExpression().toString();
                        ready[r++] = addressIndexMaker.indexFromRowColumn(i, j);
                    }
                }
            }
        }
        log.info("Expressions done " + r);

        for (byte i = 0; i < MAX_ROWS; i++) {
            for (int j = 0; j < size; j++) {
                String s;
                if (null == sps[i][j]) {
                    s = genNumOperand();
                    ready[r++] = addressIndexMaker.indexFromRowColumn(i, j);
                } else {
                    s = sps[i][j];
                }
                writer.write(s + (j != size - 1 ? "," : "\n"));
                if (exWriter != null) {
                    exWriter.write(toExcel(s) + (j != size - 1 ? "," : "\n"));
                }
            }
            sps[i] = null;
            System.out.println((char) (i + 'A') + " row is written");
        }
        System.out.println("Done");
    }
}
