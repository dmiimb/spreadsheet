package spreadsheet.reader;

public interface Reader {
    String[] readLine() throws SpreadsheetReaderException;
}
