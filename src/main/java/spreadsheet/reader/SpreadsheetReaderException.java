package spreadsheet.reader;

public class SpreadsheetReaderException extends Exception {
    SpreadsheetReaderException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
