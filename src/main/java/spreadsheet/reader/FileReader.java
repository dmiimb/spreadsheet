package spreadsheet.reader;

import lombok.RequiredArgsConstructor;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;

@RequiredArgsConstructor
public class FileReader implements Reader {
    private final String filename;
    private BufferedReader reader;

    @Override
    public String[] readLine() throws SpreadsheetReaderException {
        try {
            String line = reader().readLine();
            return null == line
                    ? null
                    : line.split(",");
        } catch (IOException e) {
            throw new SpreadsheetReaderException("File read error", e);
        }

    }

    private BufferedReader reader() throws SpreadsheetReaderException {
        if (null == reader) {
            try {
                reader = new BufferedReader(new java.io.FileReader(filename));
            } catch (FileNotFoundException e) {
                throw new SpreadsheetReaderException("File open error", e);
            }
        }
        return reader;
    }
}
