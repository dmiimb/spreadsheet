package spreadsheet;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import spreadsheet.cell.ExpressionCellFactory;

@RequiredArgsConstructor
@Component
class SpreadsheetFactory {
    private final AddressIndexMakerFactory addressIndexMakerFactory;
    private final ExpressionCellFactory expressionCellFactory;

    Spreadsheet create() {
        return new Spreadsheet(addressIndexMakerFactory, expressionCellFactory);
    }
}
