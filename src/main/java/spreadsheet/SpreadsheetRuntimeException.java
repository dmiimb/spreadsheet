package spreadsheet;

class SpreadsheetRuntimeException extends RuntimeException {
    SpreadsheetRuntimeException(Throwable throwable) {
        super(throwable);
    }
}
