package spreadsheet;

import org.springframework.stereotype.Component;

@Component
public class AddressIndexMakerFactory {
    public AddressIndexMaker create(int size) {
        return new AddressIndexMaker(size);
    }
}
