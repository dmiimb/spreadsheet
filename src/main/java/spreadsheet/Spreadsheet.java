package spreadsheet;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import spreadsheet.cell.ExpressionCell;
import spreadsheet.cell.ExpressionCellFactory;
import spreadsheet.cell.InvalidExpressionException;
import spreadsheet.reader.Reader;
import spreadsheet.reader.SpreadsheetReaderException;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Log4j2
@RequiredArgsConstructor
public class Spreadsheet {
    public final static byte MAX_ROWS = 26;
    private final AddressIndexMakerFactory addressIndexMakerFactory;
    private final ExpressionCellFactory expressionCellFactory;
    private int size;
    private byte rows;
    private Double[][] valueTable;
    private List<ExpressionCell> cells;
    private AddressIndexMaker addressIndexMaker;

    public Double getValueByAddress(int address) {
        int[] pos = addressIndexMaker.decode(address);
        return valueTable[pos[0]][pos[1]];
    }

    void importData(Reader src) throws SpreadsheetException {
        String[] line = read(src);
        if (null == line) {
            initSpreadsheet(0);
            return;
        }
        initSpreadsheet(line.length);

        rows = 0;
        // sparse array of expressions to be processed by map-reduce lately
        ExpressionCell[] sparseExpressionCellList = new ExpressionCell[MAX_ROWS * size];

        try {
            addLine(rows++, line, sparseExpressionCellList);
            while ((line = read(src)) != null) {
                addLine(rows++, line, sparseExpressionCellList);
            }
        } catch (SpreadsheetRuntimeException e) {
            throw new SpreadsheetException("Failed to parse the source", e.getCause());
        }

        // repack expressions into list
        cells = Arrays.stream(sparseExpressionCellList)
                .parallel()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        log.info("Repacking finished");
    }

    private String[] read(Reader src) throws SpreadsheetException {
        try {
            return src.readLine();
        } catch (SpreadsheetReaderException e) {
            throw new SpreadsheetException("Source reading error", e);
        }
    }

    private void initSpreadsheet(int size) {
        this.size = size;
        valueTable = new Double[MAX_ROWS][size];
        addressIndexMaker = addressIndexMakerFactory.create(size);
    }

    private void addLine(byte row, String[] cells, ExpressionCell[] cellList) throws SpreadsheetException {
        if (cells.length != size) {
            throw new SpreadsheetException("Lines length are not equal");
        }
        IntStream.range(0, size)
                .parallel()
                .forEach(i -> {
                    String cell = cells[i];
                    if ('=' == cell.charAt(0)) {
                        int index = addressIndexMaker.indexFromRowColumn(row, i);
                        try {
                            ExpressionCell e = createExpressionCell(addressIndexMaker, index, cell.substring(1));
                            if (e.getMissingReferencesCnt() > 0) {
                                cellList[index] = e;
                            } else {
                                valueTable[row][i] = e.getValue(this);
                            }
                        } catch (SpreadsheetException | InvalidExpressionException ex) {
                            throw new SpreadsheetRuntimeException(ex);
                        }
                    } else {
                        valueTable[row][i] = Double.parseDouble(cell);
                    }
                });
        log.info("Added line " + (row + 1));
    }

    private ExpressionCell createExpressionCell(AddressIndexMaker addressIndexMaker, int index, String expression)
            throws SpreadsheetException {
        try {
            return expressionCellFactory.create(addressIndexMaker, index, expression);
        } catch (InvalidExpressionException e) {
            throw new SpreadsheetException("Failed parsing expression", e);
        }
    }

    Double[][] getEvaluatedTable() throws SpreadsheetException, SpreadsheetLoopException {
        try {
            evaluate();
            log.info("Done");
            return Arrays.copyOfRange(valueTable, 0, rows);
        } catch (SpreadsheetRuntimeException e) {
            throw new SpreadsheetException("Failed to parse the source", e.getCause());
        }
    }

    private void evaluate() throws SpreadsheetLoopException {
        int prevSize = cells.size();
        log.info("Start calculating: " + prevSize);
        while (prevSize > 0) {
            cells.stream()
                    .parallel()
                    .forEach(cell -> {
                        cell.reCheckReferences(this);
                        if (0 == cell.getMissingReferencesCnt()) {
                            int[] pos = addressIndexMaker.decode(cell.getAddress());
                            try {
                                valueTable[pos[0]][pos[1]] = cell.getValue(this);
                            } catch (InvalidExpressionException ex) {
                                throw new SpreadsheetRuntimeException(ex);
                            }
                        }
                    });
            cells = cells.stream()
                    .parallel()
                    .filter(ExpressionCell::isNotCalculated)
                    .collect(Collectors.toList());
            if (prevSize == cells.size()) {
                String list = cells.stream()
                        .map(ExpressionCell::getAddress)
                        .map(a -> addressIndexMaker.decodeToString(a))
                        .collect(Collectors.joining(", "));
                throw new SpreadsheetLoopException("Cycled references detected: " + list);
            }
            prevSize = cells.size();
            log.info("To recalculate: " + prevSize);
        }
    }
}
