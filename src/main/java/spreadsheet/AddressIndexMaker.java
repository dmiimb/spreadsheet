package spreadsheet;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AddressIndexMaker {
    private final int size;

    public int indexFromRowColumn(byte row, int zeroColumn) {
        return row * size + zeroColumn;
    }

    public int indexFromCellName(char row, int column) {
        return indexFromRowColumn((byte) (row - 'A'), column - 1);
    }

    public int[] decode(int index) {
        return new int[]{
                index / size,
                index % size,
        };
    }

    public String decodeToString(int index) {
        int[] pos = decode(index);
        return (char) (pos[0] + 'A') + String.valueOf(pos[1] + 1);
    }
}
