package spreadsheet.writer;

public class SpreadsheetWriterException extends Exception {
    SpreadsheetWriterException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
