package spreadsheet.writer;

public interface Writer {
    void write(Double[][] table) throws SpreadsheetWriterException;
}
