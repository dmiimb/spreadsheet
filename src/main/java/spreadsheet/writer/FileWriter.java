package spreadsheet.writer;

import lombok.RequiredArgsConstructor;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class FileWriter implements Writer {
    private static final String NUMBER_FORMAT = "%.5f";
    private final String filename;

    @Override
    public void write(Double[][] table) throws SpreadsheetWriterException {
        System.out.println("Writing file...");
        try (BufferedWriter writer = new BufferedWriter(new java.io.FileWriter(filename))) {
            for (Double[] row : table) {
                String strRow = Arrays.stream(row)
                        .parallel()
                        .map(d -> String.format(NUMBER_FORMAT, d))
                        .collect(Collectors.joining(","));
                writer.write(strRow + "\n");
            }
        } catch (IOException e) {
            throw new SpreadsheetWriterException("File write error", e);
        }
    }
}
