package spreadsheet;

class SpreadsheetException extends Exception {
    SpreadsheetException(String s) {
        super(s);
    }

    SpreadsheetException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
