package spreadsheet;

class SpreadsheetLoopException extends Exception {
    SpreadsheetLoopException(String s) {
        super(s);
    }
}
