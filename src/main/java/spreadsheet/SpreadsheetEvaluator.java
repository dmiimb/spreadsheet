package spreadsheet;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import spreadsheet.reader.Reader;
import spreadsheet.writer.SpreadsheetWriterException;
import spreadsheet.writer.Writer;

@Component
@RequiredArgsConstructor
class SpreadsheetEvaluator {
    private final SpreadsheetFactory spreadsheetFactory;

    void evaluate(Reader source, Writer destination) throws SpreadsheetException, SpreadsheetLoopException {
        long startTime = System.nanoTime();
        Spreadsheet sp = spreadsheetFactory.create();
        sp.importData(source);
        try {
            Double[][] table = sp.getEvaluatedTable();
            System.out.println("Calculation done in " + (float) (System.nanoTime() - startTime) / 1000000 + " ms");
            destination.write(table);
        } catch (SpreadsheetWriterException e) {
            throw new SpreadsheetException("Writing to destination", e);
        }
    }
}
