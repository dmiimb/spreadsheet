# How to run
1. Build the jar file: <br />`mvn package`
2. Run: <br />
`java -jar target/spreadsheet.jar -i inputfile.csv -o outputfile.csv`

# How to generate test file
`java -jar target/spreadsheet.jar -g columns -er expression_rate -o outputfile.csv [--excel]`

As a result, the output file (specified by `-o`) will have 26 rows (A-Z) and `columns` columns.

If `--excel` parameter is given, there will be two files generated. The second file will have _reverted references_ like
normal excel spreadsheets (A5 -> E1).

`expression_rate` is a percentage of expressions in the output file

# Use cases
```
> mvn package

> java -jar target/spreadsheet.jar -g 100000 -er 10 -o target/sample_input.csv

> java -jar target/spreadsheet.jar -i target/sample_input.csv -o target/sample_output.csv
```

For large input files memory limit might need to be increased: 
```
> java -jar target/spreadsheet.jar -g 5000000 -er 1 -o target/sample_input.csv

> java -Xmx10240m -jar target/spreadsheet.jar -i target/sample_input.csv -o target/sample_output.csv
```
The last example takes about 100 seconds in total on Intel i7 @ 2.70GHz.